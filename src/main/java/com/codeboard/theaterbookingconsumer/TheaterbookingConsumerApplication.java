package com.codeboard.theaterbookingconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheaterbookingConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheaterbookingConsumerApplication.class, args);
	}

}
