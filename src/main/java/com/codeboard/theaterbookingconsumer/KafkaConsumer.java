package com.codeboard.theaterbookingconsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	private EmailSenderService emailSenderService;

	@KafkaListener(topics = "helloTopic", groupId = "consumer-group")
	public String consume(String email) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("lnin7820@gmail.com");
		message.setTo(email);
		message.setText(" Movie Booked Successfully");
		message.setSubject("Movie Booking");
		javaMailSender.send(message);
//		emailSenderService.sendEmail(email);
		
		System.out.println("Message : " + message);
		return email;
	}
//	@KafkaListener(topics="helloTopic",groupId="consumer-group")
//	public void consume(String message) {
//		System.out.println("Message : "+message);
//	}
}
