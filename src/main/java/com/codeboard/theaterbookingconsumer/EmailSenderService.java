package com.codeboard.theaterbookingconsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {
	
	@Autowired
	private JavaMailSender javaMailSender;

//	public void sendEmail(String email) {
//		MimeMessagePreparator messagePreparator = mimeMessage -> {
//	        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
//	        messageHelper.setFrom("lnin7820@gmail.com");
//	        messageHelper.setReplyTo(email);
//	        messageHelper.setTo(email);
//	        messageHelper.setSubject("Booked");
//	        messageHelper.setText("Movie Booked ");
//	    };
//		javaMailSender.send(messagePreparator);
//	}
	
	public void sendEmail(String email) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("lnin7820@gmail.com");
		message.setTo(email);
		message.setText("Movie booked Successfully");
		message.setSubject("Movie Booked");
		javaMailSender.send(message);
		
		System.out.println("Mail Sent Successfully...");
	}
}
